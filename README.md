Задание:
Написать расширения для категории NSMutableArray, позволяющие выполнить данные операции:
 	а) поменять местами элементы массива по указанным индексам. Пример метода:
     	- (void)swapObjectAtIndex:(NSUInteger)firstIndex withObjectAtIndex:(NSUInteger)secondIndex;
 	б) отсортировать массив с помощью алгоритма Шелла. Пример метода:
     	- (void)shellSortWithKey:(NSString *)key;
         Пример использования:
     	[array shellSortWithKey:@"integerValue"];