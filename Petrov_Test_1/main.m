//
//  main.m
//  Petrov_Test_1
//
//  Created by Anton Petrov on 11/17/16.
//  Copyright © 2016 Anton Petrov. All rights reserved.
//

#import <Foundation/Foundation.h>

/*а) поменять местами элементы массива по указанным индексам. Пример метода:
- (void)swapObjectAtIndex:(NSUInteger)firstIndex withObjectAtIndex:(NSUInteger)secondIndex;
б) отсортировать массив с помощью алгоритма Шелла. Пример метода:
- (void)shellSortWithKey:(NSString *)key;
Пример использования:
[array shellSortWithKey:@"integerValue"];*/

@interface NSMutableArray (PetrovTestCategory)

- (void)swapObjectAtIndex:(NSUInteger)firstIndex withObjectAtIndex:(NSUInteger)secondIndex;

- (void)shellSortWithKey:(NSString *)key;

@end

@implementation NSMutableArray (PetrovTestCategory)

- (void)swapObjectAtIndex:(NSUInteger)firstIndex withObjectAtIndex:(NSUInteger)secondIndex {
    
    if(firstIndex > self.count - 1 || secondIndex > self.count - 1) {
        NSLog(@"Attempt to enter incorrect index");
        exit(1);
    }
    NSMutableArray* tmp = self[firstIndex];
    self[firstIndex] = self[secondIndex];
    self[secondIndex] = tmp;
}

- (void)shellSortWithKey:(NSString *)key {
    
    NSUInteger d = [self count] / 2;
    while(d > 1) {
        for(int i = 0; i < self.count - d; i++)
        {
            if([[self objectAtIndex:i] valueForKey:key] > [[self objectAtIndex:i + d] valueForKey:key]) {
                [self swapObjectAtIndex:i withObjectAtIndex:i+d];
            }
        }
        d = d / 2;
    }
    for(int i = 1; i < self.count; i++) {
        id tmp = [self objectAtIndex:i];
        int item = i - 1;
        while(item >=0 && [[self objectAtIndex:item] valueForKey:key] > [tmp valueForKey:key]) {
            [self swapObjectAtIndex:item+1 withObjectAtIndex:item];
            item --;
        }
    }
}
@end

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
    }
    return 0;
}
